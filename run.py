import sys
import traceback

from fsl_siena.parser import parse_config
from fsl_siena.main import run
from fsl_siena.xnat_logging import stdout_log, stderr_log

def main():
    """Main module"""
    # Parse input args
    cmd_list=parse_config()    
    # run Siena
    run(cmd_list)

if __name__ == "__main__":
    try:
        main()
    except Exception as exp:
        traceback_info = traceback.format_exc()
        stdout_log.error("There was an exception: %s Check stderr.log for more details. \n",exp)
        stderr_log.error("Exception: %s %s \n",exp,traceback_info)
        sys.exit(1)
