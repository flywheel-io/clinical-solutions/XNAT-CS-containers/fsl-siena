import subprocess
import sys
import os
import xnat
from .xnat_logging import stdout_log

def check_options(cmd_list,cmd_options):
    """Checks if user provided options are available

    Args:
        cmd_list (list): command to run
        cmd_options (list): user provided options
    """
    # run the command, by default it will return the help menu
    process = subprocess.Popen(
                    cmd_list,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.PIPE,
                    universal_newlines=True
                )
            
    stdout, stderr = process.communicate()
    process.wait()

    # Check stdout and then stderr, since commands can log 'help' to either log.
    # check if user provided options exist:
    if stdout:
        for opt in cmd_options:
            if opt not in stdout:
                stdout_log.error(f"{opt} is not a possible option for {''.join(cmd_list)}")
                sys.exit(1)
        
    if stderr:
        for opt in cmd_options:
            if opt not in stderr:
                stdout_log.error(f"{opt} is not a possible option for {''.join(cmd_list)}")
                sys.exit(1)

def get_nifti_file(nifti_dir,scanID,sessionID):
    """Get input nifti image path

    Args:
        nifti_dir (str): path to search under
        scanID (str): scan id
        sessionID (str): session id
    Returns:
        str: string path to NIFTI file 
    """
    
    if not os.path.exists(nifti_dir):
        stdout_log.error(f"Scan: {scanID} from session: {sessionID} does not have a NIFTI resource!")
        sys.exit(1)
    else:   
        nifti_resource_files= os.listdir(nifti_dir)
        nii_files = [file for file in nifti_resource_files if ".nii.gz" in file or ".nii" in file]

        # Assuming that there is only 1 file under NIFTI resource. 
        # This should be the case for T1, T2 datasets
        if len(nii_files) > 0:
            nifti_file = os.path.join(nifti_dir,nii_files[0])
            return nifti_file
        elif len(nii_files) == 0:
            stdout_log.error(f"No files under NIFTI resource for Scan: {scanID} from session: {sessionID}")
            sys.exit(1)


def get_ventricle_mask(project,mask_file,resource="SIENA"):
    """Get ventricle mask

    Args:
        project (str): project id
        mask_file (str): ventricle mask full file name
        resource (str, optional): resource name. Defaults to "SIENA".

    Returns:
        str: path to ventricle mask in container
    """
    tmp="/tmp"
    host=os.environ["XNAT_HOST"]
    user=os.environ["XNAT_USER"]
    password=os.environ["XNAT_PASS"]

    with xnat.connect(host, user=user, password=password) as session:
        proj_resources=session.get_json(f"/data/projects/{project}/files")
        
        found_file=False
        found_rsrc=False

        # look for the specified file in project resources
        for result in proj_resources['ResultSet']['Result']:
            if result["collection"] == resource:
                found_rsrc=True

                if result['Name'] == mask_file:
                    file_uri=result['URI']
                    if file_uri:
                        session.download(file_uri, f"{tmp}/{mask_file}")
                        
                        if os.path.exists(f"{tmp}/{mask_file}"):
                            stdout_log.info(f"Found and copied {mask_file} to {tmp}")
                            found_file=True
                            return f"{tmp}/{mask_file}"
                        else:
                            stdout_log.error(f"Unable to copy {mask_file} to {tmp}")
                            sys.exit(1)

    if not found_rsrc:
        stdout_log.error(f"Unable to find the {resource} resource under project resources!")
        sys.exit(1)

    if not found_file:
        stdout_log.error(f"Unable to find {mask_file} under the {resource} project resource!")
        sys.exit(1)


def extract_metrics(report,metric):
    """Extract metric to text file

    Args:
        report (str): path to the report.siena or report.viena file
        metric (str): metric name. Either can be finalPVVC or finalPBVC
    """

    stdout_log.info(f"Parsing {report} for {metric}...")
    output_path= os.path.dirname(report)

    # Read in report
    with open(report, "r") as file:
        lines = file.readlines()

    # Go through each line of file, reversed
    for line in reversed(lines):
        if line.startswith(f"final{metric}"):
            # Extract the metric value
            metric_value = line.split()[1]

            metric_file= os.path.join(output_path,f"{metric}.txt")
            # Write to "{metric}.txt"
            with open(metric_file, "w") as output_file:
                output_file.write(f"Estimated {metric}: " + metric_value)
            break

    if os.path.exists(metric_file):
        stdout_log.info(f"Saved {metric} to {metric_file}")
    else:
        stdout_log.error(f"There was an error in saving {metric} to {metric_file}!")
        sys.exit(1)
