"""Parser module to parse command line args from command.json"""
import os
import argparse
import datetime
from .util import check_options, get_nifti_file, get_ventricle_mask

parser = argparse.ArgumentParser()

parser.add_argument('--scan1_mnt', required=True,help="Scan1 mount")
parser.add_argument('--scan2_mnt', required=True,help="Scan2 mount")

parser.add_argument('--project', required=True,help="Project ID")

parser.add_argument('--output_dir', required=True,help="Output directory")
parser.add_argument('--session1', required=True,help="Session 1")
parser.add_argument('--session2', required=True,help="Session 2")
parser.add_argument('--scan1', required=True,help="Scan 1")
parser.add_argument('--scan2', required=True,help="Scan 2")

parser.add_argument('-t2', required=False, type=str,help="Tell FAST that input is T2w, not T1w")
parser.add_argument('-d', required=False, action='store_true',help="Don't delete intermediate files")
parser.add_argument('-2', required=False, action='store_true',help="Do single class segmentation")
parser.add_argument('-m', required=False, action='store_true',help="Use standard-space masking as well as BET")
parser.add_argument('-V', required=False, action='store_true',help="Run ventricle analysis")

parser.add_argument('-v', required=False, type=str,help="Ventricle mask")
parser.add_argument('-B', required=False, type=str,help="BET options")
parser.add_argument('-S', required=False, type=str,help="Siena_diff options")
parser.add_argument('-t', required=False, type=float,help="Ignore t mm from top")
parser.add_argument('-b', required=False, type=float,help="Ignore b mm from bottom")

args=parser.parse_args() 

def parse_config():
    """Parse inputs and config
    Returns:
        list: siena command and options as a list
    """

    # siena command
    cmd_list=["siena"]

    session1ID=args.session1.split("/")[-1]
    session2ID=args.session2.split("/")[-1]

    scan1ID=args.scan1.split("/")[-1]
    scan2ID=args.scan2.split("/")[-1]

    # find the two nifti files and their full path to it
    nifti_file1=get_nifti_file(f"{args.scan1_mnt}/NIFTI",scan1ID,session1ID)
    nifti_file2=get_nifti_file(f"{args.scan2_mnt}/NIFTI",scan2ID,session2ID)
    cmd_list.extend([nifti_file1,nifti_file2])

    # Output parent folder e.g, {session1ID}_{scan1ID}_to_{session2ID}_{scan2ID}
    parent_folder=f"{session1ID}_scan-{scan1ID}_to_{session2ID}_scan-{scan2ID}"

    timestamp = datetime.datetime.now()
    timestamp_folder = timestamp.strftime("%m-%d-%Y_%H:%M:%S")

    output_folder=os.path.join(args.output_dir,parent_folder,timestamp_folder)
    cmd_list.extend(["-o",output_folder])

    # check options in BET_options. If they're invalid, then throw error.
    BET_options=args.B
    if BET_options:
        # Parse bet_opts to extract options
        options_in_bet_opts = []
        skip_next = False
        for opt in BET_options.split():
            if skip_next:
                skip_next = False
                continue
            # special case with -g. it can accept negative values, -g -0.3. -0.3 would be considered an option if we only check for "-".
            if opt.startswith("-g"):
                options_in_bet_opts.append(opt)
                skip_next = True
            elif opt.startswith("-"):
                options_in_bet_opts.append(opt.strip())

        check_options(["bet"],options_in_bet_opts)
        cmd_list.extend(["-B",BET_options])

    # check options in siena_diff_options. If they're invalid, then throw error.
    siena_diff_options=args.S
    if siena_diff_options:
        options_in_siena_diff_opts = [opt.strip() for opt in siena_diff_options.split() if opt.startswith("-")]
        check_options(["siena_diff"],options_in_siena_diff_opts)
        cmd_list.extend(["-S",siena_diff_options])

    # Get Ventricle mask file
    ventricle_mask=args.v
    if ventricle_mask:
        ventricle_mask=ventricle_mask.strip()
        ventricle_mask_path=get_ventricle_mask(args.project,ventricle_mask)
    
        cmd_list.extend(["-v",ventricle_mask_path])
            
    # Build up siena command with remaining options
    for arg in vars(args):
        argument_name = arg
        argument_value = getattr(args, arg)
        
        if isinstance(argument_value, bool):         
            if argument_value:
                cmd_arg=f"-{argument_name}"
                cmd_list.append(cmd_arg)
        
        if argument_name in ["t", "b"]:
            if argument_value:
                cmd_arg=f"-{argument_name}"
                cmd_list.extend([cmd_arg,str(argument_value)])

        if argument_name == "t2" and argument_value == "True":
            cmd_list.append("-t2")
                
    
    return cmd_list