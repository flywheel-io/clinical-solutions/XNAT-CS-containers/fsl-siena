# fsl-siena

This subject-level XNAT container wraps v6.0.7.7 FSL's [SIENA](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SIENA), which estimates the percentage brain volume change (PBVC) between two images, taken of the same subject at different time points.


- [fsl-siena](#fsl-siena)
  - [Summary](#summary)
    - [Inputs](#inputs)
    - [Config](#config)
    - [Outputs](#outputs)
    - [Citation](#citation)

## Summary
- `fsl-siena` needs two 3D NIFTI files (.nii or .nii.gz) that are saved as a scan resource under the NIFTI folder. The container will specifically look for a folder called NIFTI under the scan, and assumes that there is only 1 NIFTI file (`fsl-siena` will always take the first .nii or .nii.gz file found as the input file).
-  To include the optional [Ventricle_mask](#inputs) input save the mask as a project resource under **SIENA:**
   - <img src="./images/ventricle_mask_rsrc.png" width=25% />
- If `fsl-siena` is re-run on a subject, pre-existing outputs are not overwritten. Instead any outputs are saved under their own [timestamped folder](#outputs).
- LICENSING NOTE: FSL software are owned by Oxford University Innovation and license is required for any commercial applications. For commercial licence please contact fsl@innovation.ox.ac.uk. For academic use, an academic license is required which is available by registering on the FSL website. Any use of the software requires that the user obtain the appropriate license. See https://fsl.fmrib.ox.ac.uk/fsldownloads_registration for more information.

- Performance:
    - `reserve-memory` in the command.json was set to 2000 (2GB)
    - `fsl-siena`'s performance will vary on the resolution, quality, field of view etc of the images.

### Inputs
- *Session_1*
  - __Type__: *Session*
  - __Description__: *Dropdown of all sessions*.
  - __Required__: *True*
- *Anatomical_scan1*
  - __Type__: *Scan*
  - __Description__: *Dropdown of all scans in Session_1. This scan must have a NIFTI resource*.
  - __Required__: *True*
  - __Note__: *Set T2 config to True if run on T2 image.*
- *Session_2*
  - __Type__: *Session*
  - __Description__: *Dropdown of all sessions*.
  - __Required__: *True*
- *Anatomical_scan2*
  - __Type__: *Scan*
  - __Description__: *Dropdown of all scans in Session_2. This scan must have a NIFTI resource*.
  - __Required__: *True*
  - __Note__: *Set T2 config to True if run on T2 image.*
- *Ventricle_mask*
  - __Type__: *String*
  - __Description__: *Ventricle mask full filename encapsulated by quotes. This file should be saved under the 'SIENA' project resource. e.g, 'the_ventricle_mask.nii.gz'. Corresponds to -v in siena. By default siena will use MNI152_T1_2mm_VentricleMask.nii.gz.*
- *BET_options*
  - __Type__: *String*
  - __Description__: *Options to pass to BET, encapsulated by quotes. e.g,'-f 0.3 -g 0.6'. Corresponds to -B in siena.*
- *Siena_diff_options*
  - __Type__: *String*
  - __Description__: *Options to pass to siena_diff program, that estimates change between two aligned images, encapsulated by quotes, e.g, '-s -i 20'. Corresponds to -S in siena.*
- *Ignore_top*
  - __Type__: *Number*
  - __Description__: *Ignore from t (mm) upwards in MNI152/Talairach space. If you need to ignore the top part of the head (e.g. if some subjects have the top missing and you need consistency across subjects). Corresponds to -t in siena.*
- *Ignore_bottom*
  - __Type__: *Number*
  - __Description__: *Ignore from b (mm) downwards in MNI152/Talairach space (b should probably be negative). Corresponds to -b in siena.*

### Config
- *T2*
  - __Type__: *Boolean*
  - __Required__: *True*
  - __Description__: *Tell FAST that both input scans are T2w, not T1w. Choose 'False' if both scans are T1w, 'True' if both are T2w. Corresponds to -t2 in siena.*
- *Debug*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Don't delete intermediate files. Corresponds to -d in siena.*
- *Single_class_segmentation*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Don’t segment gray and white matter, and segment as a single class (gray and white together). This is a good option if the gray-white matter contrast is poor. Corresponds to -2 in siena.*
- *Standard_space_masking*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Use standard-space masking as well as BET. If it is proving hard to get reliable brain segmentation from BET, for example if eyes are hard to segment out then register to standard space in order to use a pre-defined standard-space brain mask. Corresponds to -m in siena.*
- *Ventricle_analysis*
  - __Type__: *Boolean*
  - __Default__: *False*
  - __Description__: *Run ventricle analysis “VIENA” (quantifies ventricular siena). Corresponds to -V in siena.*


### Outputs
- Outputs are stored under the **SIENA** subject resource. Everytime `fsl-siena` is run, a new sub-folder (date & timestamped to UTC timezone) is created with the outputs pertaining to that run. The naming convention is as follows:
  - ```
    {Session1_ID}_scan-{scan1_ID}_to_{Session2_ID}_scan-{scan2_ID}/
        {Month-Day-Year}_{Hr:Min:Sec}/ 
    ```
    - <img src="./images/output_fs.png" width=25% />
  - [SIENA](https://fsl.fmrib.ox.ac.uk/fsl/fslwiki/SIENA/UserGuide#siena_-_Single-Time-Point_Estimation) wiki page describes the main output files that are saved under these folders
  - Additionally, the brain volume change metric that is displayed in the *report.html* file is extracted and saved to **PBVC.txt**. If [Ventricle_analysis ('VIENA')](#config) was enabled, then the ventricular volume change metric is extracted and saved to **PVVC.txt**.
  
### Citation

- [Smith 2002] S.M. Smith, Y. Zhang, M. Jenkinson, J. Chen, P.M. Matthews, A. Federico, and N. De Stefano. Accurate, robust and automated longitudinal and cross-sectional brain change analysis. NeuroImage, 17(1):479-489, 2002.

- [Smith 2004] S.M. Smith, M. Jenkinson, M.W. Woolrich, C.F. Beckmann, T.E.J. Behrens, H. Johansen-Berg, P.R. Bannister, M. De Luca, I. Drobnjak, D.E. Flitney, R. Niazy, J. Saunders, J. Vickers, Y. Zhang, N. De Stefano, J.M. Brady, and P.M. Matthews. Advances in functional and structural MR image analysis and implementation as FSL. NeuroImage, 23(S1):208-219, 2004.
